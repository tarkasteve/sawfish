;; -*- Mode: emacs-lisp -*-

(require 'rep)
(require 'rep.system)
(require 'rep.data.tables)
(require 'sawfish.wm.placement.smart)

(provide 'ag-placement)

(defmacro dolist (spec #!rest body)
  `(mapc (lambda (,(car spec)) ,@body)
         ,(cadr spec)))

(defun ag-dump-workspace-windows ()
  (interactive)
  (dolist (win (workspace-windows))
    (let ((finf (window-frame-dimensions win)))
      (format standard-output "'%s' [%s] Pos: %s FrameDim: %s\n" 
              (window-name win) (get-x-property win 'WM_CLASS)
              (window-position win) finf))))


;; ;; ;; AG Routines ;; ;; ;;

;; Dimensions of Vic frame dimensions for current theme: (width . height)
; FIXME: We could probably calculate this from the theme info
(defvar vic-med-dim '(360 . 306))
(defvar vic-small-dim '(184 . 162))
(defvar vic-large-dim '(712 . 594))

(defvar workstation-policy (make-table symbol-hash eq))
(table-set workstation-policy 'venue-client '(0   0 355 396))
(table-set workstation-policy 'vic-client   '(356 0 595 396))
(table-set workstation-policy 'rat-client   '(952 0 330 396))
(table-set workstation-policy 'vic-rows 3)
(table-set workstation-policy 'vic-cols 7)
(table-set workstation-policy 'vic-row-off 396)
(table-set workstation-policy 'vic-col-off 0)

(defvar quad-left-policy (make-table symbol-hash eq))
(table-set quad-left-policy 'venue-client '(0     0 473  524))
(table-set quad-left-policy 'vic-client   '(473   0 807 1024))
(table-set quad-left-policy 'rat-client   '(0   524 473  500))
(table-set quad-left-policy 'vic-rows 2)
(table-set quad-left-policy 'vic-cols 3)
(table-set quad-left-policy 'vic-row-off 0)
(table-set quad-left-policy 'vic-col-off 1280)

(defvar ag-policy workstation-policy)

;; Hackery to inject resize-key events into windows

(setq warp-to-window-enabled t)

(defun ag-inject-key (win key)
  (grab-server)

  (let ((count 0))
    (while (and (< count 500) (not (equal win (input-focus))))
      (warp-cursor-to-window win)
      (sync-server)

      (set-input-focus win)
      (raise-window win)
      (accept-x-input)
      (sync-server)

      (setq count (1+ count)))

    (if (= count 500)
        (format standard-error "Failed to grab window %S\n" (window-name win))
      (synthesize-event (lookup-event key) win)))

  (ungrab-server))

(defun ag-set-window (win key)
    (format standard-error "Got match on %s\n" (window-name win))  
    (let ((lst (table-ref ag-policy key)))
      (raise-window win)
      (window-put win 'fixed-position )
      (set-window-type win 'unframed)
      (move-window-to win (car lst) (cadr lst))
      (resize-window-to win (caddr lst) (cadddr lst))))

(defun ag-control-placement (win)
  (format standard-error "Checking window %s\n" (window-name win))
  (cond
   ((string-match "^Venue Client" (window-name win))
    (ag-set-window win 'venue-client))

   ((string-match "^vic" (nth 2 (get-x-property win 'WM_CLASS)))
    (ag-set-window win 'vic-client))

   ((string-match "^RAT" (window-name win))
    (ag-set-window win 'rat-client))))

(defun ag-place-controls ()
  (interactive)
  (dolist (win (workspace-windows))
    (ag-control-placement win)))


;; Get and sort all Vic display windows
;; FIXME: Make into a macro?
(defun all-vic-windows ()
  (let ((wins (sort (delete-if-not 
                     (lambda (x) 
                       (string-match "^vw" (nth 2 (get-x-property x 'WM_CLASS))))
                     (managed-windows))
                    (lambda (x y) 
                      (string-lessp (window-name x) (window-name y))))))
    wins))


(defun ag-tile-vic ()
  (interactive)
  (let ((row 0) (col 0))
    (dolist (win (all-vic-windows))

      (ag-inject-key win "m")
      (move-window-to win 
         (+ (table-ref ag-policy 'vic-col-off) (* col (car vic-med-dim)))
         (+ (table-ref ag-policy 'vic-row-off) (* row (cdr vic-med-dim))))

      (setq row (1+ row))
      (when (= row (table-ref ag-policy 'vic-rows))
        (setq row 0)
        (setq col (1+ col))))))

(defun ag-tile-vic-small ()
  (interactive)
   (let ((row 0) (col 0))

     (dolist (win (all-vic-windows))
       
       (ag-inject-key win "s")
       
       (move-window-to win 
         (+ (table-ref ag-policy 'vic-col-off) (* col (car vic-small-dim)))
         (+ (table-ref ag-policy 'vic-row-off) (* row (cdr vic-small-dim))))

       (setq col (1+ col))
       (when (= col (table-ref ag-policy 'vic-cols))
         (setq col 0)
         (setq row (1+ row))))))
   

(defun ag-place-all ()
  (interactive)
  (ag-place-controls)
  (ag-tile-vic))
